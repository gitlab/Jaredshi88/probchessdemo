package com.example.ndktest;
import java.util.HashMap;

public class Pieces {
    //the drawable id
    public int drawableID;
    //color of piece
    public boolean impColor;
    //id of piece
    public int impID;
    //exist boolean
    public boolean exists;

    public int col;
    public int row;


    Pieces(int rows, int columns, boolean color, int ID, boolean existence)
    {
        col=columns;
        row=rows;
        impColor=color;
        impID=ID;
        exists=existence;

        if(color&&existence) {
            switch (ID) {
                case (1):
                    drawableID=R.drawable.wpawn;
                    break;
                case (2):
                    drawableID=R.drawable.wknight;
                    break;
                case (3):
                    drawableID=R.drawable.wbishop;
                    break;
                case (4):
                    drawableID=R.drawable.wrook;
                    break;
                case (5):
                    drawableID=R.drawable.wqueen;
                    break;
                case (6):
                    drawableID=R.drawable.wking;
                    break;
            }
        }else if(color==false&&existence)
            switch (ID)
            {
                case (1):
                    drawableID=R.drawable.bpawn;
                    break;
                case (2):
                    drawableID=R.drawable.bknight;
                    break;
                case (3):
                    drawableID=R.drawable.bbishop;
                    break;
                case (4):
                    drawableID=R.drawable.brook;
                    break;
                case (5):
                    drawableID=R.drawable.bqueen;
                    break;
                case (6):
                    drawableID=R.drawable.bking;
                    break;

            }
    }

}

package com.example.ndktest;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ndktest.databinding.ActivityMainBinding;

import java.util.List;
import java.util.Vector;

public class GameBoard extends AppCompatActivity {


    // Used to load the 'ndktest' library on application startup.

public static ProgressBar WhiteChance;
public static TextView wtext;
public static ProgressBar BlackChance;
public static TextView btext;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_board);
        WhiteChance = findViewById(R.id.progressBarW);
        BlackChance = findViewById(R.id.progressBarB);
        wtext = findViewById(R.id.WhiteProbText);
        btext = findViewById(R.id.BlackProbText);
    }

    /*
    start and quit functions to allocate/delete a board instance on the heap. Attach these to a play button or something

        Note: when compiling, there is already an initial new instance of Board(), so you don't need to call startGame() unless
        you quit and want to restart.
     */

    public native void resetGame();

    /*
        the pieceid player wants to promote to (should ignore if the
        piece can't promote yet)
     */




    public native String process();

    /*
    Calling the test vector method (NOTE: only for testing purposes;
    don't look too deeply into this)
     */
    public static native Vector<Float> getVector();

    private boolean start = true;

    //key of map called in c++
    private static int retKeyT = 2;

    //current position of piece (piece you click and want to move)
    private static int current = 4;

    //proposed new position of piece
    private static int target = 5;

    //everything necessary to display I think (I'll document what is in this list tomorrow)




    //Calls verifymovestarget in c++ to make sure the move is valid and then changes the boardstate/also, depending on the mode, calls the AI to move
    //public native void verification(int modetype);

    //toggles coinflip on or off. Not sure if this is all we need
    public native void toggleCoinFlip();
public static void updateProgressBar(int white, int black){
    WhiteChance.setProgress(white);
    BlackChance.setProgress(black);
    wtext.setText(Integer.toString(white)+"%");
    btext.setText(Integer.toString(black)+"%");
}



    /**
     * A native method that is implemented by the 'ndktest' native library,
     * which is packaged with this application.
     */

}


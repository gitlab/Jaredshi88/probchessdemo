package com.example.ndktest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class TutorialPage extends AppCompatActivity {
    private ImageButton backbutton;
    private ImageButton forwardButton;
    // @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_page);
        //Set up back button to Main menu
        backbutton = (ImageButton) findViewById(R.id.imageButton3);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainpage();
            }
        });

    }
    //Function to open Main Menu
    public void openMainpage(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    //Function to open Coin Gamemode Page

}

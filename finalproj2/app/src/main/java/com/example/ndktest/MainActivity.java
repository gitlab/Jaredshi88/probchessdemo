package com.example.ndktest;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    static {
        System.loadLibrary("ndktest");
    }
    public native void resetGame();

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            resetGame();
            return true;

        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_main);
        Button mainButton = findViewById(R.id.angry_btn);

        @SuppressLint({"MissingInflatedId", "LocalSuppress"}) Button tutorialButton = findViewById(R.id.tutorialButton);
        @SuppressLint({"MissingInflatedId", "LocalSuppress"}) Button aiButton = findViewById(R.id.AIButton);


        mainButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                resetGame();
                startActivity(new Intent(MainActivity.this, GameBoard.class));

            }
        });
        tutorialButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TutorialPage.class));
            }
        });
        aiButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                resetGame();
                startActivity(new Intent(MainActivity.this, AiGame.class));
            }
        });


        //binding.sampleText.setText(process());
        //binding.sampleText.setText(result.toString());

        //binding.sampleText.setText(getState().toString());
        //binding.sampleText.setText(getProbs().toString());
        //System.out.println(getProbs()[1]);

        //promotion();

    }

}
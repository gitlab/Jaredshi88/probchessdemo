package com.example.ndktest;

import java.util.HashSet;
import java.util.Set;


public class ChessGame {

    Set<Pieces> gameBoard = new HashSet<>();

    public ChessGame()
    {
        reset();
    }
    public void reset()
    {

        gameBoard.add(new Pieces(0,7,false,4,true));
        gameBoard.add(new Pieces(7,7,true,4,true));
        gameBoard.add(new Pieces(0,0,false,4,true));
        gameBoard.add(new Pieces(7,0,true,4,true));
        gameBoard.add(new Pieces(0,6,false,2,true));
        gameBoard.add(new Pieces(7,6,true,2,true));
        gameBoard.add(new Pieces(0,1,false,2,true));
        gameBoard.add(new Pieces(7,1,true,2,true));
        gameBoard.add(new Pieces(0,5,false,3,true));
        gameBoard.add(new Pieces(7,5,true,3,true));
        gameBoard.add(new Pieces(0,2,false,3,true));
        gameBoard.add(new Pieces(7,2,true,3,true));


        gameBoard.add(new Pieces(0,3,false,5,true));
        gameBoard.add(new Pieces(0,4,false,6,true));
        gameBoard.add(new Pieces(7,3,true,5,true));
        gameBoard.add(new Pieces(7,4,true,6,true));

        for(int i =0; i<8;i++)
        {
            gameBoard.add(new Pieces(1,i,false,1,true));
            gameBoard.add(new Pieces(6,i,true,1,true));
        }
    }

    public Pieces pieceAt(int row, int col)
    {
        for(Pieces i : gameBoard)
        {
            if (i.col==col && i.row==row)
            {
                return i;
            }
        }
        return null;
    }

    public void movePiece(int iRow, int iCol, int fRow, int fCol)
    {
        if(pieceAt(iRow,iCol)!=null)
        {
            Pieces movingPiece=pieceAt(iRow,iCol);
            movingPiece.row=fRow;
            movingPiece.col=fCol;
        }



    }
}

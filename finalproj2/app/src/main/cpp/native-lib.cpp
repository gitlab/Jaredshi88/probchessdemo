#include <jni.h>

#ifndef NDKTEST_PIECE_H
#define NDKTEST_PIECE_H
#include <string>
#include <vector>

class Piece
{
public:
    Piece(): exist(false), id(0) {}
    // default constructor for an empty piece which populates the empty squares of the board

    Piece(bool inpColor, int inpId): exist(true), color(inpColor), id(inpId), moveCount(0) {}
    // constructor for a standard piece
    // input inpColor:

    std::vector<int> getMoves()
    {
        // effectively a data repository of every single possible movement offset a given piece type could make

        switch (id)
        {
            case 1:
                // pawn moves
                if (color)
                {
                    return {8, 16, 7, 9};
                }
                else
                {
                    return {-8, -16, -7, -9};
                }
            case 2:
                // knight moves
                return {-17, -15, -10, -6, 6, 10, 15, 17};
            case 3:
                // bishop moves
                return {-9, -18, -27, -36, -45, -54, -63, 9, 18, 27, 36, 45, 54, 63, -7, -14, -21, -28, -35, -42, -49,
                        7, 14, 21, 28, 35, 42, 49};
            case 4:
                // rook moves
                return {-1, -2, -3, -4, -5, -6, -7, 1, 2, 3, 4, 5, 6, 7, -8, -16, -24, -32, -40, -48, -56, 8, 16, 24,
                        32, 40, 48, 56};
            case 5:
                // queen moves
                return {-9, -18, -27, -36, -45, -54, -63, 9, 18, 27, 36, 45, 54, 63, -7, -14, -21, -28, -35, -42, -49,
                        7, 14, 21, 28, 35, 42, 49, -1, -2, -3, -4, -5, -6, -7, 1, 2, 3, 4, 5, 6, 7, -8, -16, -24, -32,
                        -40, -48, -56, 8, 16, 24, 32, 40, 48, 56};
            case 6:
                // king moves
                return {-9, -8, -7, -2, -1, 1, 2, 7, 8, 9};
            default:
                return {};
        }
    }

    std::string display()
    {
        // turns a piece into its string representation for use with Board.display()

        if (!exist)
        {
            return "  ";
        }
        if (color)
        {
            switch (id)
            {
                case 1:
                    return "Pb";
                case 2:
                    return "Nb";
                case 3:
                    return "Bb";
                case 4:
                    return "Rb";
                case 5:
                    return "Qb";
                case 6:
                    return "Kb";
                default:
                    return "  ";
            }
        }
        else
        {
            switch (id)
            {
                case 1:
                    return "Pw";
                case 2:
                    return "Nw";
                case 3:
                    return "Bw";
                case 4:
                    return "Rw";
                case 5:
                    return "Qw";
                case 6:
                    return "Kw";
                default:
                    return "  ";
            }
        }
    }

    // getters

    bool exists()
    {
        return exist;
    }

    bool getColor()
    {
        return color;
    }

    int getId()
    {
        return id;
    }

    int getMoveCount()
    {
        return moveCount;
    }

    void setId(int inpId)
    {
        id = inpId;
    }

    void incMoveCount()
    {
        moveCount++;
    }


protected:
    bool exist, color;
    // color = 0: white
    // color = 1: black

    // exist = 0: doesn't exist
    // exist = 1: does exist

    int id, moveCount;
    // id = 1: pawn
    // id = 2: knight
    // id = 3: bishop
    // id = 4: rook
    // id = 5: queen
    // id = 6: king

};

#endif //NDKTEST_PIECE_H

#ifndef NDKTEST_BOARD_H
#define NDKTEST_BOARD_H

#include "Piece.h"
#include <map>
#include <iostream>
#include <cmath>
#include <random>
#include <ctime>

class Board
{
public:
    Board(int inpId = 0): mode(inpId), turn(0)
    {
        // constructor for board object
        // input inpId: integer corresponding to which board setup you'd like. im thinking of adding more setups for other modes, but by default it is the standard chess layout

        for (int ii = 0; ii < 64; ii++)
        {
            boardState[ii] = Piece();
        }
        switch (inpId)
        {
            default:
                // standard chess board
                boardState[0] = Piece(1, 4);
                boardState[1] = Piece(1, 2);
                boardState[2] = Piece(1, 3);
                boardState[3] = Piece(1, 5);
                boardState[4] = Piece(1, 6);
                boardState[5] = Piece(1, 3);
                boardState[6] = Piece(1, 2);
                boardState[7] = Piece(1, 4);
                boardState[8] = Piece(1, 1);
                boardState[9] = Piece(1, 1);
                boardState[10] = Piece(1, 1);
                boardState[11] = Piece(1, 1);
                boardState[12] = Piece(1, 1);
                boardState[13] = Piece(1, 1);
                boardState[14] = Piece(1, 1);
                boardState[15] = Piece(1, 1);
                boardState[48] = Piece(0, 1);
                boardState[49] = Piece(0, 1);
                boardState[50] = Piece(0, 1);
                boardState[51] = Piece(0, 1);
                boardState[52] = Piece(0, 1);
                boardState[53] = Piece(0, 1);
                boardState[54] = Piece(0, 1);
                boardState[55] = Piece(0, 1);
                boardState[56] = Piece(0, 4);
                boardState[57] = Piece(0, 2);
                boardState[58] = Piece(0, 3);
                boardState[59] = Piece(0, 5);
                boardState[60] = Piece(0, 6);
                boardState[61] = Piece(0, 3);
                boardState[62] = Piece(0, 2);
                boardState[63] = Piece(0, 4);
                break;
            case (1):
                // pawn test
                boardState[36] = Piece(0, 1);
                break;
            case (2):
                // knight test
                boardState[36] = Piece(0, 2);
                break;
            case (3):
                // bishop test
                boardState[36] = Piece(0, 3);
                break;
            case (4):
                // rook test
                boardState[36] = Piece(0, 4);
                break;
            case (5):
                // queen test
                boardState[36] = Piece(0, 5);
                //boardState[35] = Piece(1, 1);
                break;
            case (6):
                // king test
                boardState[36] = Piece(0, 6);
                boardState[32] = Piece(0, 4);
                boardState[39] = Piece(0, 4);
                break;
            case (7):
                // any other wacky board combo you want
                break;

        }
    }

    std::string display()
    {
        // string representation of the board for the pre front end stage

        std::string s;
        for (int ii = 0; ii < 17; ii++)
        {
            if (!(ii%2))
            {
                s += "  -----------------------------------------\n";
            }
            else
            {
                s += std::to_string(8-(ii/2)) + " |";
                for (int jj = 0; jj < 8; jj++)
                {
                    s += " " + boardState[(8 * (ii/2)) + jj].display() + " |";
                }
                s += "\n";
            }
        }
        s += "    A    B    C    D    E    F    G    H\n";
        return s;
    }

    std::string moveDisplay(int currentKey)
    {
        // string representation of an input piece's valid moves

        std::string s;
        std::vector<int> vMoves = verifyMoves(currentKey);
        if (vMoves.empty())
        {
            return "No Valid Moves";
        }
        for (int ii = 0; ii < 17; ii++)
        {
            if (!(ii%2))
            {
                s += "  -----------------------------------------\n";
            }
            else
            {
                s += std::to_string(8-(ii/2)) + " |";
                for (int jj = 0; jj < 8; jj++)
                {
                    s += " ";
                    for (int kk = 0; kk < vMoves.size(); kk++)
                    {
                        if (vMoves[kk] == ((8 * (ii / 2)) + jj))
                        {
                            s += "XX";
                            break;
                        }
                        else if (kk + 1 == vMoves.size())
                        {
                            s += "  ";
                        }
                    }
                    s += +" |";
                }
                s += "\n";
            }
        }
        s += "    A    B    C    D    E    F    G    H\n";
        return s;
    }

    std::vector<int> verifyMoves(int currentKey)
    {
        // iterates through every possible move, checking against the general rules and then piece specific rules to validate or discard moves
        // input currentKey: integer corresponding to the piece you are trying to move

        std::vector<int> gottenMoves = boardState[currentKey].getMoves();
        // retrieves list of every possible move difference
        std::vector<int> possibleMoves, verifiedMoves;
        for (int gottenMove : gottenMoves)
        {
            possibleMoves.push_back(gottenMove+currentKey);
            // creates new list of destination positions "possible moves"
        }
        bool prevMoveValid = true;
        // previous move test: if the previous move didn't work, then this move won't work (suitable mostly for b, r, and q)
        bool prevMoveCapture = false;
        for (int ii = 0; ii < possibleMoves.size(); ii++)
        {
            if (ii % 7 == 0)
                // taking advantage of the structure of the possible moves list, a piece can only possibly move 7 spaces in any direction
                // if it reaches that seventh move and restarts the search in a different direction, it won't eliminate moves based on the
                // previous move test.
            {
                prevMoveCapture = false;
                prevMoveValid = true;
            }
            int horizontalOffset = (gottenMoves[ii] % 8);
            // horizontal offset that the move being tested would incur
            int verticalOffset = (gottenMoves[ii] / 8);
            // horizontal offset that the move being tested would incur
            int horizontalPos = (possibleMoves[ii] % 8);
            // horizontal position of the move destination being tested
            int verticalPos = (possibleMoves[ii] / 8);
            // vertical position of the move destination being tested
            int currentHorizontalPos = (currentKey % 8);
            // horizontal position of the current piece's horizontal position
            int currentVerticalPos = (currentKey / 8);
            // vertical position of the current piece's vertical position
            Piece atTarget = boardState[possibleMoves[ii]];
            // copy of the piece that is at the target (for efficiency)
            if (boardState[currentKey].getColor() != turn)
            {
                continue;
            }
            if (possibleMoves[ii] > 63 or possibleMoves[ii] < 0)
                // checking to see if it is in range of the board
            {
                prevMoveValid = false;
                continue;
            }
            if (atTarget.exists() and (atTarget.getColor() == turn))
                // checking if the move in question lands on one of your own pieces
            {
                prevMoveValid = false;
                continue;
            }
            switch (boardState[currentKey].getId())
                // piece specific checks
            {
                case 1:
                    // pawn checks
                    if (!prevMoveValid and ii < 2)
                    {
                        continue;
                    }
                    if (horizontalOffset==0 and boardState[possibleMoves[ii]].exists() and (boardState[possibleMoves[ii]].getColor() != turn))
                        // checking directly ahead (cant capture forward)
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    if (((verticalPos) == (currentVerticalPos)) or ((horizontalPos != currentHorizontalPos) and (abs(possibleMoves[ii] / 8 - (currentKey / 8)) > 1)))
                        // preventing wrap around illegal moves
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    if (((abs(gottenMoves[ii]) / 8) > 1) and (boardState[currentKey].getMoveCount() > 0))
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    if (horizontalOffset!=0 and !boardState[possibleMoves[ii]].exists())
                        // checking if there is a piece
                    {
                        bool cond1 = boardState[possibleMoves[ii]+((!turn)?8:-8)].getId() == 1;
                        // checks to make sure square adjacent is a pawn
                        bool cond2 = boardState[possibleMoves[ii]+((!turn)?8:-8)].getMoveCount() == 1;
                        // checks to make sure said pawn has only moved once
                        bool cond3 = ((verticalPos) == ((!turn)?2:5));
                        // checks if it is in the correct row
                        bool cond4 = possibleMoves[ii]+((!turn)?8:-8) == prevMoveKey;
                        // checks to make sure it was the pawn that may be en passantable that was the last piece to move
                        if (cond1 and cond2 and cond3 and cond4)
                            // checking en passant
                        {
                            enPassantTest = possibleMoves[ii];
                            break;
                        }
                        else
                        {
                            prevMoveValid = false;
                            continue;
                        }
                    }
                    break;
                case 2:
                    // knight checks
                    if ((abs((verticalPos) - (currentVerticalPos)) + abs((horizontalPos - currentHorizontalPos))) != 3)
                        // checks if euclidean distance is 3
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    break;
                case 3:
                    // bishop checks
                    if (!prevMoveValid)
                        // previous move valid test
                    {
                        continue;
                    }
                    if (prevMoveCapture)
                    {
                        continue;
                    }
                    if ((abs((verticalPos) - (currentVerticalPos)) + abs((horizontalPos - currentHorizontalPos))) % 2 != 0)
                        // bishop logic checking if the euclidean distance is divisible by two, eliminating any wrapping
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    break;
                case 4:
                    // rook checks
                    if (!prevMoveValid)
                        // previous move valid test
                    {
                        continue;
                    }
                    if (prevMoveCapture)
                    {
                        continue;
                    }
                    if (((possibleMoves[ii] < (currentKey - currentHorizontalPos)) or (possibleMoves[ii] > (currentKey - currentHorizontalPos + 7))) and (currentHorizontalPos - horizontalPos != 0))
                        // rook logic checking horizontal movement (vertical movement is easily covered by range check in the beginning
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    break;
                case 5:
                    // queen checks
                    if (!prevMoveValid)
                        // previous move valid test
                    {
                        continue;
                    }
                    if (prevMoveCapture)
                    {
                        continue;
                    }
                    // queen logic is largely a combination of rook and bishop logic
                    if ((abs(gottenMoves[ii]) < 7) or (gottenMoves[ii] % 8 == 0))
                        // checking rook moves logic
                    {
                        if (((possibleMoves[ii] < (currentKey - currentHorizontalPos)) or (possibleMoves[ii] > (currentKey - currentHorizontalPos + 7))) and (currentHorizontalPos - horizontalPos != 0))
                        {
                            prevMoveValid = false;
                            continue;
                        }
                    }
                    else if (((gottenMoves[ii] % 7 == 0) and (abs(gottenMoves[ii]) > 7)) or (gottenMoves[ii] % 9 == 0))
                        // checking bishop moves logic
                    {
                        if ((abs((verticalPos) - (currentVerticalPos)) + abs((horizontalPos - currentHorizontalPos))) % 2 != 0)
                        {
                            prevMoveValid = false;
                            continue;
                        }
                    }
                    break;
                case 6:
                    // king checks
                    if (boardState[currentKey].getMoveCount() > 0 and (gottenMoves[ii] == -2 or gottenMoves[ii] == 2))
                        // if the king has moved, then it won't be able to castle
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    if (gottenMoves[ii] == -2)
                        // testing castle long requirements (empty space between king and rook, rook hasn't moved)
                    {
                        castleLong = (!boardState[currentKey-1].exists() and !boardState[currentKey-2].exists() and !boardState[currentKey-3].exists() and (boardState[currentKey-4].getId() == 4) and (boardState[currentKey-4].getColor() == turn) and (boardState[currentKey-4].getMoveCount() == 0));
                    }
                    if (gottenMoves[ii] == 2)
                        // testing castle requirements (empty space between king and rook, rook hasn't moved)
                    {
                        castle = (!boardState[currentKey+1].exists() and !boardState[currentKey+2].exists() and (boardState[currentKey+3].getId() == 4) and (boardState[currentKey+3].getColor() == turn) and (boardState[currentKey+3].getMoveCount() == 0));
                    }
                    if (gottenMoves[ii] == -2 and !castleLong)
                        // if castle long is invalid, then it eliminates the move
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    if (gottenMoves[ii] == 2 and !castle)
                        // if castle is invalid, then it eliminates the move
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    if ((abs((verticalPos) - (currentVerticalPos)) + abs((horizontalPos - currentHorizontalPos))) > 2)
                        // invalidates move if euclidean distance is more than 2
                    {
                        prevMoveValid = false;
                        continue;
                    }
                    break;
            }
            // if it makes it through every check without entering a fail condition, it gets added to the list of verified moves
            prevMoveValid = true;
            verifiedMoves.push_back(possibleMoves[ii]);
            if (boardState[possibleMoves[ii]].getColor() == !turn and boardState[possibleMoves[ii]].exists())
            {
                prevMoveCapture = true;
            }
        }
        return verifiedMoves;
    }

    void reset()
    {
        for (int ii = 0; ii < 64; ii++)
        {
            boardState[ii] = Piece();
        }
        boardState[0] = Piece(1, 4);
        boardState[1] = Piece(1, 2);
        boardState[2] = Piece(1, 3);
        boardState[3] = Piece(1, 5);
        boardState[4] = Piece(1, 6);
        boardState[5] = Piece(1, 3);
        boardState[6] = Piece(1, 2);
        boardState[7] = Piece(1, 4);
        boardState[8] = Piece(1, 1);
        boardState[9] = Piece(1, 1);
        boardState[10] = Piece(1, 1);
        boardState[11] = Piece(1, 1);
        boardState[12] = Piece(1, 1);
        boardState[13] = Piece(1, 1);
        boardState[14] = Piece(1, 1);
        boardState[15] = Piece(1, 1);
        boardState[48] = Piece(0, 1);
        boardState[49] = Piece(0, 1);
        boardState[50] = Piece(0, 1);
        boardState[51] = Piece(0, 1);
        boardState[52] = Piece(0, 1);
        boardState[53] = Piece(0, 1);
        boardState[54] = Piece(0, 1);
        boardState[55] = Piece(0, 1);
        boardState[56] = Piece(0, 4);
        boardState[57] = Piece(0, 2);
        boardState[58] = Piece(0, 3);
        boardState[59] = Piece(0, 5);
        boardState[60] = Piece(0, 6);
        boardState[61] = Piece(0, 3);
        boardState[62] = Piece(0, 2);
        boardState[63] = Piece(0, 4);
        turn = 0;
        gameOver = false;
        castle = false;
        castleLong = false;
        coinFlipActive = false;
        prevMoveKey = -1;
        enPassantTest = -1;
        whiteMoves = 0;
        blackMoves = 0;
        whiteProbability = 10;
        blackProbability = 10;
        whitePiecesRem = 16;
        blackPiecesRem = 16;
        numPossibleMoves = 0;
        dead.clear();

    }

    void verifyMoveTarget(int currentKey, int targetKey)
    {
        // calls the verifyMove function before executing a move command
        // input currentKey: integer corresponding to the piece you are trying to move
        // input targetKey: integer corresponding to the pieces intended destination

        std::vector<int> moves = verifyMoves(currentKey);
        if (moves.empty() or (boardState[currentKey].getColor() != turn))
            // if its not your turn, or if the selected piece has no valid moves, it fails
        {
            std::cout << "invalid move!\n";
            std::cout << display();
            return;
        }
        for (int move : moves)
        {
            if (move == targetKey)
                // checks if any of the moves match with the inputted target
            {
                std::cout << "\ncurrentKey: " << currentKey << "\ntargetKey: " << targetKey << "\n\n";
                makeMove(currentKey, targetKey);
            }
        }
        //std::cout << "invalid move!\n";
        //std::cout << display();
        // catch all if the move is invalid
    }

    void makeMove(int currentKey, int targetKey, bool probabilityOverride = 0, bool success = 1)
    // executes a move regardless of whether it is verified or not
    // input currentKey: integer corresponding to the piece you are trying to move
    // input targetKey: integer corresponding to the pieces intended destination
    // inputs probabilityOverride and success: bools that allow specification of
    // success or failure for ai state generation

    {
        if (boardState[targetKey].exists() or enPassantTest == targetKey)
            // already ruled out pieces of same color, so if it exists, it's capturable
        {
            if (probabilityOverride?success:chance(currentKey))
            {
                dead.push_back(boardState[targetKey]);
                // add to killed list
                if (boardState[targetKey].getId() == 6)
                    // if the king dies, the game ends
                {
                    gameOver = true;
                }
                if (enPassantTest == targetKey)
                    // executes en passant if the target move is en passant-able
                {
                    dead.push_back(boardState[targetKey + ((!turn) ? 8 : -8)]);
                    boardState[targetKey + ((!turn) ? 8 : -8)] = Piece();
                    enPassantTest = 64;
                }
                /*if (boardState[currentKey].getId() == 1 and (targetKey / 8 == (!turn?0:7)))
                    // executes promotion if target move is promote-able
                {
                    promotion(boardState[currentKey]);
                }*/

                // remaining logic for making moves / captures
                prevMoveKey = targetKey;
                boardState[currentKey].incMoveCount();
                boardState[targetKey] = boardState[currentKey];
                boardState[currentKey] = Piece();
                ((!turn)?whiteProbability:blackProbability) = 10;
            }
            else
            {
                if (((!turn)?whiteProbability:blackProbability) > 100)
                {
                    ((!turn)?whiteProbability:blackProbability) = 100;
                }
                ((!turn)?whiteProbability:blackProbability) /= 2;
            }
        }

        if (!boardState[targetKey].exists() and enPassantTest != targetKey)
        {
            if (castleLong and targetKey == (currentKey - 2))
                // executes castling long if target move is castle long-able
            {
                boardState[targetKey + 1] = boardState[targetKey - 2];
                boardState[targetKey - 2] = Piece();
                castleLong = false;
            }
            if (castle and targetKey == (currentKey + 2))
                // executes castling if target move is castle-able
            {
                boardState[targetKey - 1] = boardState[targetKey + 1];
                boardState[targetKey + 1] = Piece();
                castle = false;
            }
            /*if (boardState[currentKey].getId() == 1 and (targetKey / 8 == (!turn ? 0 : 7)))
                // executes promotion if target move is promote-able
            {
                promotion(boardState[currentKey]);
            }*/

            int modifier = 10;

            if (((!turn)?whitePiecesRem:blackPiecesRem) < 9)
            {
                modifier *= 2;
            }

            if (coinFlipActive)
            {
                if (coinFlip())
                {
                    modifier *= 2;
                }
                else
                {
                    modifier *= 0;
                }
            }

            ((!turn)?whiteProbability:blackProbability) += modifier;

            // remaining logic for making moves / captures
            prevMoveKey = targetKey;
            boardState[currentKey].incMoveCount();
            boardState[targetKey] = boardState[currentKey];
            boardState[currentKey] = Piece();
        }
        if (!mode)
        {
            turn = !turn;
        }
        //std::cout << display();
        //std::cout << "\nWhite Prob: " << whiteProbability << "\nBlack Prob: " << blackProbability << "\n";
        return;
    }

    void promotion(int key, int selection)
    // method that promotes a piece
    // input &p: Piece object reference to be changed
    {
        boardState[key].setId(selection);
    }

    bool chance(int currentKey)
    // chance method that changes this from chess to probability chess by deciding the chance a capture is realized
    // input currentKey: integer corresponding to the piece you are trying to make a capture with
    {
        srand(time(nullptr));
        int likelihood = ((!turn)?whiteProbability:blackProbability);
        int id = boardState[currentKey].getId();
        switch (id)
            // list of probability modifiers corresponding to each piece.
        {
            case 1:
                likelihood += 10;
                break;
            case 2: case 3:
                likelihood += 20;
                break;
            case 4:
                likelihood += 30;
                break;
            case 5:
                likelihood += 60;
                break;
            case 6:
                likelihood += 80;
                break;
            default:
                break;
        }
        int tester = (rand() % 100);
        if (tester < likelihood)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool coinFlip()
    // code that just returns 50/50 true or false
    {
        srand(time(nullptr));
        int tester = (rand() % 2);
        return (tester != 0);
    }

    void toggleCoinFlip()
    {
        coinFlipActive = !coinFlipActive;
    }

    std::map<int,std::pair<int,std::vector<int>>> generateAllMoves()
    // generates every possible move that could be made
    {
        numPossibleMoves = 0;
        std::map<int,std::pair<int,std::vector<int>>> outp;
        std::vector<int> pieceKeys;
        for (int ii = 0; ii < 64; ii++)
        {
            if (boardState[ii].exists() and boardState[ii].getColor() == turn)
            {
                pieceKeys.push_back(ii);
            }
        }
        int trueIndex = 0;
        for (int index = 0; index < pieceKeys.size(); index++)
        {
            std::vector<int> moves = verifyMoves(pieceKeys[index]);
            if (moves.size() > 0)
            {
                outp[trueIndex] = std::make_pair(pieceKeys[index], moves);
                numPossibleMoves += moves.size();
                trueIndex++;
            }
        }
        return outp;
    }

    double getPieceScore()
    {
        // ok im spaghettifying this code DONT FORGET TO CHECK TURN ON GAME OVER WHEN I GET TO THE LAB
        if (gameOver)
        {
            return -std::numeric_limits<double>::infinity();
        }
        int score = 0;
        for (int ii = 0; ii < 64; ii++)
        {
            int val;
            switch (boardState[ii].getId())
            {
                default:
                    val = 0;
                    break;
                case 1:
                    val = 1;
                    break;
                case 2:
                    val = 3;
                    break;
                case 3:
                    val = 3;
                    break;
                case 4:
                    val = 5;
                    break;
                case 5:
                    val = 9;
                    break;
            }
            score += (val * ((boardState[ii].getColor()^turn)?-1:1));
        }
        return score;
    }


    std::map<int,Piece> boardState;
    // the map holding the current state of the board

    std::vector<Piece> dead;
    // list of pieces that have been captured

    bool turn, gameOver = false;
    // turn 0: white to move
    // turn 1: black to move
    // gameOver 0: game not over
    // gameOver 1: game over

    bool castle = false, castleLong = false;
    // board state booleans allowing a proposed move to castle or not

    bool coinFlipActive = false;
    // boolean corresponding to an unimplemented function that asks the player if they want to coin flip

    int mode;
    // the board setup

    int prevMoveKey = 0, enPassantTest = -1;
    // en passant helping int fields

    int whiteMoves = 0, blackMoves = 0;
    // integers corresponding to the total number of moves played for each side

    int whiteProbability = 10, blackProbability = 10;
    // integers corresponding to the probability pool for each side

    int whitePiecesRem = 16, blackPiecesRem = 16;
    // integers corresponding to the number of pieces remaining on each side

    int numPossibleMoves = 0;

protected:

};

#endif //NDKTEST_BOARD_H


#ifndef NDKTEST_STATE_H
#define NDKTEST_STATE_H

#include <utility>

class State
{
public:
    State(Board b, State* pred, std::pair<int,int> inpMove): currentBoard(std::move(b)), pPredecessor(pred), score(0), moveMade(std::move(inpMove)){}

    std::vector<State> generateStates()
    // generates every possible move that could be made into a new state
    {
        std::vector<State> outp;
        if (currentBoard.gameOver)
        {
            return outp;
        }

        std::map<int,std::pair<int,std::vector<int>>> moves = currentBoard.generateAllMoves();
        for (int ii = 0; ii < moves.size(); ii++)
        {
            for (int jj = 0; jj < 2 * moves[ii].second.size(); jj++)
            {
                Board nextBoard = currentBoard;
                nextBoard.makeMove(moves[ii].first, moves[ii].second[jj / 2], 1, jj % 2);
                outp.push_back(*new State(nextBoard, this, std::make_pair(moves[ii].first, moves[ii].second[jj / 2])));
            }
        }
    }


    double score;
    std::pair<int,int> moveMade;
    Board currentBoard;
    State* pPredecessor;
    std::vector<State> possibleStates;
};

#endif //PROBABILITYCHESS_STATE_H



#ifndef NDKTEST_AI_H
#define NDKTEST_AI_H

#include <algorithm>

class AI
{
public:
    AI(State inpState, int inpMode = 0, int inpDepthLimit = 1): currentState(inpState), mode(inpMode), depthLimit(inpDepthLimit) {}

    std::pair<int,int> pickMove()
    {
        switch (mode)
        {
            case 0:
                return randomMove();
            case 1:
                return calculatedMove();
            default:
                return std::make_pair(-1, -1);
        }
    }

    std::pair<int,int> randomMove()
    {
        // picks a random piece that has valid moves, then randomly selects a move that piece can make
        srand(time(NULL));
        std::map <int, std::pair <int, std::vector <int> > > moves = currentState.currentBoard.generateAllMoves();
        int currentKey = 0, targetKey = 0;
        int mSize = moves.size();
        int indexOne = (rand() % mSize);
        currentKey = moves[indexOne].first;
        int cmSize = moves[indexOne].second.size();
        int indexTwo = (rand() % cmSize);
        targetKey = moves[indexOne].second[indexTwo];
        std::pair<int,int> move (currentKey, targetKey);
        return move;
    }

    std::pair<int,int> calculatedMove()
    {
        // uses the score generated to pick the "best" next move
        std::vector<State> states = currentState.generateStates();
        int maxIndex = 0;
        int maxScore = 0;
        for (int ii = 0; ii < states.size(); ii++)
        {
            int score = generateScore(states[ii], depthLimit-1);
            if (score > maxScore)
            {
                maxScore = score;
                maxIndex = ii;
            }
            std::cout << "\ncurrentKey: " << states[ii].moveMade.first << "\ntargetKey: " << states[ii].moveMade.second << "\nscore: " << score << "\n\n";
        }
        return states[maxIndex].moveMade;
    }

    double boardScore(State s)
    // this function would, if i had the time, do more than just call the getPieceScore() method
    // such as analyzing piece locations and whatnot
    {
        return s.currentBoard.getPieceScore();
    }

    double generateScore(State s, int depth)
    // recursively generates a score based on the moves it could make in the future
    // input s: State object to be analyzed
    // input depth: int that corresponds to the depth limit
    {
        if (depth == 0)
        {
            double score = this->boardScore(s);
            std::cout << "\nin: generateScore() score: " << score << "\n\n";
            return score;
        }
        else
        {
            std::vector<State> states = s.generateStates();
            std::vector<double> scores;
            for (int ii = 0; ii < states.size(); ii++)
            {
                scores.push_back(generateScore(states[ii], (depth - 1)));

            }
            return -*std::max_element(scores.begin(), scores.end());
        }
    }
    int mode, depthLimit;
    // if i had made more ai's, this would be more prevalent, but it just corresponds to either random or calculated

    State currentState;
    // State object that it uses to generate the next move
};

#endif //GROUP8PROJECT_AI_H

/*
 * This is NOT a permanent solution: place all the objects in a class where the board is only called once.
 * Note to self/others: it will remain here only for testing
 */
Board* board = new Board();

/*
 * Create a new instance of the game when player presses start (NOT FINISHED)
 * If this doesn't work, don't need to create a new board on heap;
 * we can just have the start button overwrite board every time it's pressed
 */
extern "C" JNIEXPORT void JNICALL Java_com_example_ndktest_ChessView_startGame(JNIEnv* env,jobject mainActivityInstance /* this */) {

    board = new Board();

}

/*
 * Delete the instance of the game when gameover or quit (NOT FINISHED)
 */
extern "C" JNIEXPORT void JNICALL Java_com_example_ndktest_MainActivity_resetGame(JNIEnv* env,jobject mainActivityInstance /* this */) {
    board->reset();
}


/*
 * TESTING: Create a string in c++ and send it to java to be displayed. A very simple introduction
 * for you other backend guys to get what's going on
 */
extern "C" JNIEXPORT jstring JNICALL Java_com_example_ndktest_GameBoard_stringFromJNI(JNIEnv* env,jobject /* this */) {
    std::string hello = "sent from C++";
    return env->NewStringUTF(hello.c_str());
}

/*
 * TESTING: receive a string from method "ProcessInJava" and concatenate. Proceed to send back to java
 * Very Good way to review how the method below works
 */
extern "C" JNIEXPORT jstring JNICALL Java_com_example_ndktest_GameBoard_process(JNIEnv* env,jobject mainActivityInstance /* this */) {
    jclass mainActivityCls = env->GetObjectClass(mainActivityInstance);
    jmethodID jmethodId = env->GetMethodID(mainActivityCls,"processInJava","()Ljava/lang/String;");

    if (jmethodId == nullptr){
        return env->NewStringUTF("");
    }

    jobject result = env->CallObjectMethod(mainActivityInstance,jmethodId);
    std::string java_msg = env->GetStringUTFChars((jstring) result,JNI_FALSE);
    std::string c_msg = "Result from Java ";
    std::string msg = c_msg + java_msg;

    return env->NewStringUTF(msg.c_str());

}

extern "C" JNIEXPORT jobject JNICALL Java_com_example_ndktest_GameBoard_getVector(JNIEnv* env,jclass mainActivityInstance /* this */) {

    std::vector<float> testvar = {14.2,15,24};

    //find the class where the method is. NOTE: retired strategy
    //jclass mainActivityCls = env->GetObjectClass(mainActivityInstance);

    jclass jVectorClass = env->FindClass("java/util/Vector");
    jclass jFloatClass = env->FindClass("java/lang/Float");

    //find the method ID of the void function where the parameters will be changed in the class
    jmethodID vectorConstructorID = env->GetMethodID(jVectorClass,"<init>", "()V");
    jmethodID floatConstructorID = env->GetMethodID(jFloatClass,"<init>","(F)V");
    jmethodID addMethodId = env->GetMethodID(jVectorClass,"add", "(Ljava/lang/Object;)Z");

    /*
     * Unnecessary, but playing it safe I think? <- Past me is dumb
     */
    if (jVectorClass == NULL ||
        jFloatClass == NULL || vectorConstructorID == NULL ||
        floatConstructorID == NULL || addMethodId == NULL){
        return NULL;
    }

    jobject vecreturn = env->NewObject(jVectorClass,vectorConstructorID);

    for (float f: testvar){

        jobject floatValue = env->NewObject(jFloatClass,floatConstructorID,f);

        if (floatValue == NULL){
            return NULL;
        }

        env->CallBooleanMethod(vecreturn,addMethodId,floatValue);
    }

    //Best way to prevent memory leak maybe?? Not sure if removing the class is right
    env->DeleteLocalRef(jVectorClass);
    env->DeleteLocalRef(jFloatClass);

    return vecreturn;
}

extern "C" JNIEXPORT jobject JNICALL Java_com_example_ndktest_ChessView_getState(JNIEnv* env,jobject mainActivityInstance /* this */,jint rows,jint cols) {
/*
    retKeyT is the key that java calls for the map it wants
    convert the 2 ints to a 0-63 key for the backend map
    */
    jint retKeyT = rows*8 + cols;

    //find the class where the method is.
    jclass mainActivityCls = env->GetObjectClass(mainActivityInstance);

    jclass jVectorClass = env->FindClass("java/util/Vector");
    jclass jIntClass = env->FindClass("java/lang/Integer");
    jclass jListClass = env->FindClass("java/util/ArrayList");
    jclass jBoolClass = env->FindClass("java/lang/Boolean");

    //find the method ID of the void function where the parameters will be changed in the class

    jmethodID vectorConstructorID = env->GetMethodID(jVectorClass,"<init>", "()V");
    jmethodID listConstructorID = env->GetMethodID(jListClass,"<init>","()V");


    jmethodID intConstructorID = env->GetMethodID(jIntClass,"<init>","(I)V");
    jmethodID boolConstructorID = env->GetMethodID(jBoolClass,"<init>", "(Z)V");

    jmethodID addvMethodId = env->GetMethodID(jVectorClass,"add","(Ljava/lang/Object;)Z");
    jmethodID addMethodId = env->GetMethodID(jListClass,"add", "(Ljava/lang/Object;)Z");


    /*
     * Unnecessary, but playing it safe I think? <- Past me is dumb
     */
    if (jVectorClass == NULL ||
        vectorConstructorID == NULL ||
        addMethodId == NULL ||
        jIntClass == NULL || jListClass == NULL ||
        jBoolClass == NULL || listConstructorID == NULL ||
        intConstructorID == NULL || boolConstructorID == NULL || addvMethodId == NULL){
        return NULL;
    }

    /*
     * Ignore. I'm keeping this here just in case I get some inspiration to do it more efficiently
     */
    //if (board.verifyMoveTarget(interpretMove(current),interpretMove(target)) == true)

    //instantiate new java vector and list objects to be filled
    jobject vecreturn = env->NewObject(jVectorClass,vectorConstructorID);
    jobject listReturn = env->NewObject(jListClass,listConstructorID);

    //Checks whose turn it is
    jobject BoolTurn = env->NewObject(jBoolClass,boolConstructorID,board->turn);

    //Add whose turn it is to list
    env->CallBooleanMethod(listReturn,addMethodId,BoolTurn);

    //Checks if there is a piece on this square
    jobject BoolExists = env->NewObject(jBoolClass,boolConstructorID,board->boardState[retKeyT].exists());

    //Add exists to list
    env->CallBooleanMethod(listReturn,addMethodId,BoolExists);

    //color for key [result]
    jobject BoolColor = env->NewObject(jBoolClass,boolConstructorID,board->boardState[retKeyT].getColor());

    //add the color to list
    env->CallBooleanMethod(listReturn,addMethodId,BoolColor);

    //piece id for key [result]
    jobject intValue = env->NewObject(jIntClass,intConstructorID,board->boardState[retKeyT].getId());

    //add the piece id to the list
    env->CallBooleanMethod(listReturn,addMethodId,intValue);


    for (int f: board->verifyMoves(retKeyT)){

        jobject intKeyVal = env->NewObject(jIntClass,intConstructorID,f);

        //create a java verifymoves vector
        env->CallBooleanMethod(vecreturn,addvMethodId,intKeyVal);
    }

    //add the java verifymoves to the list
    env->CallBooleanMethod(listReturn,addMethodId,vecreturn);

    jobject WhiteProb = env->NewObject(jIntClass,intConstructorID,board->whiteProbability);
    env->CallBooleanMethod(listReturn,addMethodId,WhiteProb);

    jobject BlackProb = env->NewObject(jIntClass,intConstructorID,board->blackProbability);
    env->CallBooleanMethod(listReturn,addMethodId,BlackProb);

    //Can someone do a lookover and check if I deleted everything? There is a lot I'm putting on the heap
    env->DeleteLocalRef(jVectorClass);
    env->DeleteLocalRef(jIntClass);
    env->DeleteLocalRef(jListClass);
    env->DeleteLocalRef(jBoolClass);

    return listReturn;
}

//This is for calling the verifymoves function for each square the player chooses to make a move at
extern "C" JNIEXPORT void JNICALL Java_com_example_ndktest_ChessView_verification(JNIEnv* env,jobject mainActivityInstance /* this */,jint gamemode, jint rowsc, jint colsc,jint rowst,jint colst) {

    switch (gamemode) {
        case 0:

            ///Case 0 is for AI vs player. If it is the AI's turn, then create a new state on the heap For the AI to simulate a turn. Currently there is a memory leak;
            /// please fix backend guys!

            if(board->turn){
                State currentState = *new State(*board,nullptr,std::make_pair(-1,-1));
                AI playerTwo = AI(currentState,0);
                std::pair<int,int> move = playerTwo.pickMove();
                int current = move.first;
                int target = move.second;
                board->verifyMoveTarget(current,target);
            }
            else{
                jint current = rowsc*8+colsc;
                jint target = rowst*8+colst;

                board->verifyMoveTarget(current,target);

            }

            break;

            ///Case 1 is the normal probability game which will call verifymovetarget for moves called by java for both
            ///players

        case 1:

            jint current = rowsc*8+colsc;
            jint target = rowst*8+colst;

            board->verifyMoveTarget(current,target);
            break;
    }
}

/*
 * Retrieve the probability scores of white and black just in case we want to display that on the board
 */

/*
extern "C" JNIEXPORT jintArray JNICALL Java_com_example_ndktest_ChessView_getProbs(JNIEnv* env,jobject mainActivityInstance /* this ) {
    //create new java array

    jintArray result = env->NewIntArray(2);

    if (result == NULL){
        return 0;
    }

    //temp array so that setarray function can be used
    jint fill[2];
    fill[0] = board->whiteProbability;
    fill[1] = board->blackProbability;

    env->SetIntArrayRegion(result,0,2,fill);

    return result;
}
*/

extern "C" JNIEXPORT void JNICALL Java_com_example_ndktest_ChessView_promotion(JNIEnv* env,jobject mainActivityInstance /* this */,jint key,jint selection) {
    board->promotion(key,selection);
}

extern "C" JNIEXPORT void JNICALL Java_com_example_ndktest_GameBoard_toggleCoinFlip(JNIEnv* env,jobject mainActivityInstance /* this */) {
    board->toggleCoinFlip();
}

extern "C" JNIEXPORT jobject JNICALL Java_com_example_ndktest_ChessView_getProbabilities(JNIEnv* env,jobject mainActivityInstance /* this */) {
    jclass jVectorClass = env->FindClass("java/util/Vector");
    jclass jIntClass = env->FindClass("java/lang/Integer");

    if (jVectorClass == NULL || jIntClass == NULL){
        return NULL;
    }

    jmethodID vectorConstructorID = env->GetMethodID(jVectorClass,"<init>", "()V");
    jmethodID intConstructorID = env->GetMethodID(jIntClass,"<init>","(I)V");
    jmethodID addvMethodId = env->GetMethodID(jVectorClass,"add","(Ljava/lang/Object;)Z");

    //empty vector to be filled with probabilities
    jobject vecreturn = env->NewObject(jVectorClass,vectorConstructorID);

    jobject WhiteProb = env->NewObject(jIntClass,intConstructorID,board->whiteProbability);
    env->CallBooleanMethod(vecreturn,addvMethodId,WhiteProb);

    jobject BlackProb = env->NewObject(jIntClass,intConstructorID,board->blackProbability);
    env->CallBooleanMethod(vecreturn,addvMethodId,BlackProb);

    env->DeleteLocalRef(jVectorClass);
    env->DeleteLocalRef(jIntClass);

    return vecreturn;
}

